var gulp  = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');


gulp.task('default', function() {
	console.log('Gulpfile working!!!');
});




gulp.task('compileJsFiles', function() {
	gulp.src([
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap/dist/js/bootstrap.min.js',
		'bower_components/angular/angular.min.js',
		'bower_components/angular-route/angular-route.min.js',

		'app/components/medicationSearchForm/medicationSearchForm.component.js',
		'app/components/medicationSearchForm/medicationSearchForm.factory.js',
		'app/components/medicationSearchForm/medicationSearchForm.controller.js',
		'app/components/medicationSearchResultList/medicationSearchResultList.component.js',
		'app/components/medicationSearchResultList/medicationSearchResultList.factory.js',
		'app/components/medicationSearchResultList/medicationSearchResultList.controller.js',
		'app/components/medicationSearchResult/medicationSearchResult.component.js',
		'app/components/medicationSearchResult/medicationSearchResult.factory.js',
		'app/components/medicationSearchResult/medicationSearchResult.controller.js',
		'app/app.module.js',
		'app/app.routes.js',
		'app/common/factories/utilityHelper.factory.js',
		'app/pages/medicationSearch/medicationSearch.controller.js'
	])
	.pipe(uglify())
	.pipe(concat('index.js'))
	.pipe(gulp.dest(''));
});