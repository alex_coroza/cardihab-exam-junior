(function() {

	angular.module('appModule', [
		'ngRoute',

		/* components */
		'medicationSearchFormComponent', 
		'medicationSearchResultListComponent', 
		'medicationSearchResultComponent'
	]);
	
	
	
	
})();