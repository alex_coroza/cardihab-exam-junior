(function() {

	angular.module('medicationSearchFormComponent')
	.factory('MedicationSearchFormFactory', medicationSearchFormFactory)

	medicationSearchFormFactory.$inject = ['$http', '$timeout'];



	function medicationSearchFormFactory($http, $timeout) {

		var factory = {
			username: '0400000000',
			password: '0400000000',
			searchTerms: 'NORPACE,AMIODARONE,CORDARONE',
			searchResults: {}, /* store here the medication searches that returns results */

			// store result counts here so we wont have to recount on every digest loop of angularJS
			nonEmptyResultsCount: 0, /* put here the count of search terms with results */
			emptyResultsCount: 0, /* put here the count of search terms with no results */
			
			getMedicationsByTerm: getMedicationsByTerm, /* api request to get medication search results by term */
			getMedicationsPerSearchTerm: getMedicationsPerSearchTerm, /* call getMedicationsByTerm for each search term supplied, and can supply delay on each api call */
		};



		return factory;




		function getMedicationsByTerm(searchTerm) {
			var authToken = localStorage.getItem('cardihabAuthToken');

			return $http({
				method: 'GET',
				url: 'https://api-dev.cardihab.app/v1/medications/search?count=100&term='+searchTerm,
				headers: {
					'Accept': 'application/json',
					'Authorization': 'Bearer '+authToken
				} 
			});
		}




		function getMedicationsPerSearchTerm(searchTerms, delayPerRequest) {
			var timeoutDuration;
			var searchTermsArray = searchTerms.split(',');

			// search medications for each searchTerms with a "delayPerRequest" ms delay for each api request
			angular.forEach(searchTermsArray, function(searchTerm, searchTermIndex) {
				timeoutDuration = delayPerRequest * searchTermIndex;

				$timeout(function() {
					getMedicationsByTerm(searchTerm).then(function(response) {
						factory.searchResults[searchTerm] = response.data;
					}, function(errorResponse) {
						if(errorResponse.status == 404) {
							factory.searchResults[searchTerm] = [];
						}
					});
				}, timeoutDuration);
			});
		}




		
		
		
		
	}
})();