(function() {

	angular.module('medicationSearchFormComponent')
	.controller('MedicationSearchFormController', medicationSearchFormController)

	medicationSearchFormController.$inject = ['MedicationSearchFormFactory', 'UtilityHelperFactory'];



	function medicationSearchFormController(MedicationSearchFormFactory, UtilityHelperFactory) {

		var vm = this;
		vm.medicationSearchForm = MedicationSearchFormFactory;

		// methods
		vm.init = init;
		vm.searchMedications = searchMedications;


		init();




		function init() {
			// init actions here
		}




		function searchMedications() {
			// reset search results when searching for new results
			MedicationSearchFormFactory.searchResults = {};
			MedicationSearchFormFactory.nonEmptyResultsCount = 0;
			MedicationSearchFormFactory.emptyResultsCount = 0;

			// request to login api to get the authToken
			UtilityHelperFactory.getAuthToken(MedicationSearchFormFactory.username, MedicationSearchFormFactory.password).then(
				function(loginResponse) {
					UtilityHelperFactory.storeAuthToken(loginResponse); /* store authToken for later use */
					MedicationSearchFormFactory.getMedicationsPerSearchTerm(MedicationSearchFormFactory.searchTerms, 1000); /* iterate through searchTerms and with 1second delay for each api request */
				}, function(errorResponse) {
					// error callback for getAuthToken
					console.log(errorResponse);
					alert(errorResponse.status + ' Error! Mobile number or password is incorrect!');
				}
			);
		}
		


		
		
		
	}
})();