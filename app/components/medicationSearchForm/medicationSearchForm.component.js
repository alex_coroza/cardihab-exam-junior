(function() {

	angular.module('medicationSearchFormComponent', []);
	
	angular.module('medicationSearchFormComponent')
	.component('medicationSearchFormComponent', {
		templateUrl: 'app/components/medicationSearchForm/medicationSearchForm.tpl.html',
		controller: 'MedicationSearchFormController as vm',
	});

	
	
	
	
})();