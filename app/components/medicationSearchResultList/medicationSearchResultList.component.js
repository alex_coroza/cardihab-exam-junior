(function() {

	angular.module('medicationSearchResultListComponent', []);
	
	angular.module('medicationSearchResultListComponent')
	.component('medicationSearchResultListComponent', {
		templateUrl: 'app/components/medicationSearchResultList/medicationSearchResultList.tpl.html',
		controller: 'MedicationSearchResultListController as vm',
	});

	
	
	
	
})();