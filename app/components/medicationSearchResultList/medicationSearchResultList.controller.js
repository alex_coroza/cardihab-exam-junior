(function() {

	angular.module('medicationSearchResultListComponent')
	.controller('MedicationSearchResultListController', medicationSearchResultListController)

	medicationSearchResultListController.$inject = ['MedicationSearchResultListFactory', 'MedicationSearchFormFactory', 'UtilityHelperFactory'];



	function medicationSearchResultListController(MedicationSearchResultListFactory, MedicationSearchFormFactory, UtilityHelperFactory) {

		var vm = this;
		vm.medicationSearchResultList = MedicationSearchResultListFactory;
		vm.medicationSearchForm = MedicationSearchFormFactory;
		vm.utilityHelper = UtilityHelperFactory;

		// methods
		vm.nonEmptyResultsCountIncrement = nonEmptyResultsCountIncrement;
		vm.emptyResultsCountIncrement = emptyResultsCountIncrement;





		function nonEmptyResultsCountIncrement() {
			MedicationSearchFormFactory.nonEmptyResultsCount++;
		}


		function emptyResultsCountIncrement() {
			MedicationSearchFormFactory.emptyResultsCount++;
		}
		
		
		
		
		
	}
})();