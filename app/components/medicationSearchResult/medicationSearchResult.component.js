(function() {

	angular.module('medicationSearchResultComponent', []);
	
	angular.module('medicationSearchResultComponent')
	.component('medicationSearchResultComponent', {
		templateUrl: 'app/components/medicationSearchResult/medicationSearchResult.tpl.html',
		controller: 'MedicationSearchResultController as vm',
	});

	
	
	
	
})();