(function() {

	angular.module('medicationSearchResultComponent')
	.controller('MedicationSearchResultController', medicationSearchResultController)

	medicationSearchResultController.$inject = ['MedicationSearchResultFactory', 'MedicationSearchFormFactory'];



	function medicationSearchResultController(MedicationSearchResultFactory, MedicationSearchFormFactory) {

		var vm = this;
		vm.medicationSearchResult = MedicationSearchResultFactory;
		vm.medicationSearchForm = MedicationSearchFormFactory;

		
		
		
		
	}
})();