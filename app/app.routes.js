(function() {

	angular.module('appModule')
	.config(configurations);

	configurations.$inject = ['$routeProvider'];


	
	function configurations($routeProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'app/pages/medicationSearch/medicationSearch.tpl.html',
			controller: 'MedicationSearchController as vm'
		})
		.otherwise({
			redirectTo: '/'
		});
		

	} /* closing configurations */
})();