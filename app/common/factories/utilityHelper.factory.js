(function() {

	angular.module('appModule')
	.factory('UtilityHelperFactory', utilityHelperFactory);

	utilityHelperFactory.$inject = ['$http'];

	

	function utilityHelperFactory($http) {

		var factory = {
			objectLength: objectLength, /* return the number of properties of an object, use this becuase .length() is for array only and not working for objects */
			getAuthToken: getAuthToken, /* send a request to Cardihab's login api to get the auth token */
			storeAuthToken: storeAuthToken, /* store authToken(from login api) to localStorage for later use */
		};


		return factory;




		function objectLength(sampleObject) {
			var length = Object.keys(sampleObject).length;
			return length;
		}




		function getAuthToken(username, password) {
			return $http.post('https://api-dev.cardihab.app/v1/login', { 
				username: username, password: password 
			});
		}
		
		
		
		
		function storeAuthToken(loginResponse) {
			var authToken = loginResponse.data.replace(/"/g, ''); /* login api returns authToken with enclosed with quotations so lets remove it before saving */
			localStorage.setItem('cardihabAuthToken', authToken); /* store token into local storage for later use */
		}




		
		
		
		

	}
})();