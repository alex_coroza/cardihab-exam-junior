* Run "npm install" to install install dev dependencies like bower,gulp,etc and then "bower install" to install the frontend dependencies. It is a good practice to gitignore the plugins' files and just include the package.json/bower.json/etc files

* In case your machine has no installed npm and bower, visit this tutorial 
      https://webdesign.tutsplus.com/tutorials/how-to-install-npm-and-bower--cms-23451

* Please run from a server to avoid "cross-origin" error when loading template files

* As for my case, i put the folder in a apache server(c:/xampp/www/htdocs) and then visited index.html

